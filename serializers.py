from rest_framework import serializers
from rest_framework.serializers import SerializerMethodField
from .models import *


class MobileNumberSerializer(serializers.ModelSerializer):
	"""docstring for MobileNumberSerializer"""
	password = serializers.CharField(write_only=True, min_length=8, max_length=40, error_messages={
	                                 "blank": "Password cannot be empty", "min_length": "Password is too short", "max_length": "Password is too long"})

	# To convert an Cloudinary Object into URL with specific dimension
	profile_image = SerializerMethodField()

	def get_profile_image(self, obj):
		if obj.user_image:
			return obj.user_image.build_url(width=100)
		return None

	# To convert an Cloudinary Object into URL with specific dimension mainly for thumb page
	thumb_image = SerializerMethodField()

	def get_thumb_image(self, obj):
		if obj.user_image:
			if obj.image_type == "Initial":
				return obj.user_image.build_url(width=60, height=60, radius="max", crop="crop", gravity="center")
			return obj.user_image.build_url(width=60, height=60, radius="max", crop="fill", gravity="face")
		return None

	#Create Fuction for creating User Object
	def create(self, validated_data):
		mobile = MobileNumber(
			otp=000000, mobile_number=validated_data['mobile_number'], country_code=validated_data['country_code'])
		mobile.set_password(validated_data['password'])
		mobile.save()
		return mobile

	class Meta:
		"""docstring for meta"""
		model = MobileNumber
		exclude = ('otp', 'otp_active', 'last_login', 'is_superuser',
		           'is_staff', 'groups', 'user_permissions')


class PaymentSerializer(serializers.ModelSerializer):
	"""docstring for PaymentSerializer"""
	class Meta:
		"""docstring for meta"""
		model = UserPaymentMethods
		fields = "__all__"


class RetrievePaymentSerializer(serializers.ModelSerializer):
    """docstring for RetrievePaymentSerializer"""
    card_number = SerializerMethodField()

    def get_card_number(self, obj):
        card_number = str(obj.card_number)
        return "************" + card_number[:4]

    class Meta:
        """docstring for meta"""
        model = UserPaymentMethods
        fields = ("id", "card_number", "payment_type",
                  "card_type", "default_card")


class ValetatSerializer(serializers.ModelSerializer):
	"""docstring for ValetatSerializer"""

	distance = SerializerMethodField()

	def get_distance(self, obj):
		return obj.distance / 1609.34

	service_for = SerializerMethodField()

	def get_service_for(self, obj):
		return obj.service_for.place_type

	crop_image = SerializerMethodField()

	def get_crop_image(self, obj):
		if obj.image:
			return obj.image.build_url(width=170, height=60, crop="fill", gravity="center")
		return None

	class Meta:
		"""docstring for meta"""
		model = ValetAt
		fields = "__all__"


class PromoCodeInviteeSerializer(serializers.ModelSerializer):
	"""docstring for PromoCodeInviteeSerializer"""

	invitee_image = SerializerMethodField()

	def get_invitee_image(self, obj):
		if obj.user_image:
			if obj.image_type == "Initial":
				return obj.user_image.build_url(width=60, height=60, radius="max", crop="crop", gravity="center")
			return obj.user_image.build_url(width=60, height=60, radius="max", crop="fill", gravity="face")
		return None

	class Meta:
		"""docstring for meta"""
		model = MobileNumber
		fields = ('first_name', 'last_name', 'invitee_image')


class PromoCodeSerializer(serializers.ModelSerializer):
	"""docstring for PromoCodeSerializer"""
	invitee = PromoCodeInviteeSerializer()

	class Meta:
		"""docstring for meta"""
		model = PromoCodeBalance
		exclude = ('id', 'user', 'created_at')


class NotificationSerializer(serializers.ModelSerializer):
	"""docstring for PromoCodeSerializer"""
	class Meta:
		"""docstring for meta"""
		model = Notification
		exclude = ("created_at", "user",)


class GetStartedRequestSerializer(serializers.ModelSerializer):
	"""docstring for GetStartedRequestSerializer"""
	class Meta:
		"""docstring for meta"""
		model = GetStartedRequest
		exclude = ("created_at",)


class PaymentProcessSerializer(serializers.ModelSerializer):
	"""docstring for PaymentSerializer"""
	class Meta:
		"""docstring for meta"""
		model = PaymentRecords
		fields = "__all__"


class PaymentRecordsSerializer(serializers.ModelSerializer):
	"""docstring for PaymentRecordsSerializer"""
	total_amount = SerializerMethodField()

	def get_total_amount(self, obj):
		return "$" + "{0:.2f}".format(float(obj.parking_fee) + float(obj.valet_tip) if obj.valet_tip else 00.00)

	valet_name = SerializerMethodField()

	def get_valet_name(self, obj):
		return "#" + str(obj.valet_request.valet.id) + " " + obj.valet_request.valet.name

	card_type = SerializerMethodField()

	def get_card_type(self, obj):
		return obj.card.card_type

	modified_date = SerializerMethodField()

	def get_modified_date(self, obj):
		return "{:%m.%d.%y @ %I:%M%p}".format(obj.created_at)

	class Meta:
		"""docstring for meta"""
		model = PaymentRecords
		exclude = ("parking_fee", "valet_tip", "user",
		           "valet_request", "card", "created_at")


class PaymentRecordDetailSerializer(serializers.ModelSerializer):
	"""docstring for PaymentRecordDetailSerializer"""

	valet_name = SerializerMethodField()

	def get_valet_name(self, obj):
		return "#" + str(obj.valet_request.valet.id) + " " + obj.valet_request.valet.name

	modified_date = SerializerMethodField()

	def get_modified_date(self, obj):
		return "{:%m.%d.%y @ %I:%M%p}".format(obj.created_at)

	image_file = SerializerMethodField()

	def get_image_file(self, obj):
		if obj.receipt_image:
			return obj.receipt_image.build_url(height=330)

	pdf_file = SerializerMethodField()

	def get_pdf_file(self, obj):
		if obj.receipt_pdf:
			return obj.receipt_pdf.build_url()

	class Meta:
		"""docstring for meta"""
		model = PaymentRecords
		exclude = ("parking_fee", "valet_tip", "user", "valet_request",
		           "card", "created_at", "id", "receipt_image", "receipt_pdf")
