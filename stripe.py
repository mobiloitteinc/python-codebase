from django.conf import settings
import stripe
from .models import *

stripe.api_key = settings.STRIPE_API_KEY

class StripePayment(object):
	"""docstring for StripePayment"""

	def __init__(self, amount,user):
		super(StripePayment, self).__init__()
		self.__card = user.payments.get(default_card=True)
		self.__amount = float(amount['parking_fee'])+float(amount['valet_tip']) if amount['valet_tip'] else 00.00

	def payment(self):
		try:
			account_charge = stripe.Charge.create(
				amount=int(self.__amount),
				currency="aud", 
				source={
					"object": 'card',
					"number": self.__card['card_number'],
					"cvc": self.__card['card_cvv'],
					"exp_month": self.__card['exp_month'],
					"exp_year": self.__card['exp_year'],
				}, 
			)
		except Exception as e:
			return self.__card.id
		return self.__card.id

		