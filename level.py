from facebook_business.adobjects.adsinsights import AdsInsights
from client_flow.tense import tense


def level(request):
    print("Request", request)
    if 'performance_level' in request:
        if request['performance_level'] == "Campaign":
            return AdsInsights.Level.campaign
        elif request['performance_level'] == "Adsets":
            return AdsInsights.Level.adset
        elif request['performance_level'] == "Ad":
            return AdsInsights.Level.ad
        else:
            return AdsInsights.Level.account
    else:
        return AdsInsights.Level.account


class Level:
    """docstring for CPC
    """

    def __init__(self, request, metric, insight):
        super(Level, self).__init__()
        self.__request = request
        self.__metric = metric
        self.__insight = insight

    def level(self):
        if 'performance_level' in self.__request:
            if self.__request['performance_level'] == "Campaign":
                return self.campaign_level()
            elif self.__request['performance_level'] == "Adsets":
                return self.adsets_level()
            elif self.__request['performance_level'] == "Ad":
                return self.ad_level()
            else:
                return self.response()
        else:
            return self.response()

    def campaign_level(self):
        value = 0
        find = None
        if self.__request.get('any', None):
            for camp in self.__insight:
                if self.__request.get('any').lower() in camp.get("campaign_name", "").lower():
                    if self.__metric == "cpr":
                        value += (float(camp.get('spend', 0)) /
                                  int(camp.get('estimated_ad_recallers', 0)))
                    elif self.__metric == "roas":
                        value += float(camp.get('mobile_app_purchase_roas', 0)) + \
                            float(camp.get('website_purchase_roas', 0))
                    elif self.__metric == "roi":
                        value += (
                            float(camp.get('cost_per_total_action', 0)) -
                            float(camp.get('spend', 0))) / float(camp.get('spend', 0))
                    else:
                        value += float(camp.get(self.__metric, 0))
                    find = camp.get('campaign_name')
                    break
            if find:
                return {
                    "payload": {
                        "slack": {
                            "text": "{}'s Facebook {} for {} campaign {} £{}".format(self.__request['period'].title(), self.__metric.upper(), find, tense(self.__request), "{0:.2f}".format(value))
                        }
                    }
                }
            else:
                return {
                    "payload": {
                        "slack": {
                            "text": "It's seems either you misspelled campaign name or your account don't have this campaign details"
                        }
                    }
                }

    def adsets_level(self):
        value = 0
        find = None
        if self.__request.get('any', None):
            for camp in self.__insight:
                if self.__request.get('any').lower() in camp.get("adset_name", "").lower():
                    print("Find")
                    if self.__metric == "cpr":
                        value += (float(camp.get('spend', 0)) /
                                  int(camp.get('estimated_ad_recallers', 0)))
                    elif self.__metric == "roas":
                        value += float(camp.get('mobile_app_purchase_roas', 0)) + \
                            float(camp.get('website_purchase_roas', 0))
                    elif self.__metric == "roi":
                        value += (
                            float(camp.get('cost_per_total_action', 0)) -
                            float(camp.get('spend', 0))) / float(camp.get('spend', 0))
                    else:
                        value += float(camp.get(self.__metric, 0))
                    find = camp.get('adset_name')
                    break
            if find:
                return {
                    "payload": {
                        "slack": {
                            "text": "{}'s Facebook {} for {} ad set {} £{}".format(
                                self.__request['period'].title(
                                ), self.__metric.upper(),
                                find,
                                tense(self.__request),
                                "{0:.2f}".format(value)
                            )
                        }
                    }
                }
            else:
                return {
                    "payload": {
                        "slack": {
                            "text": "It's seems either you misspelled ad set or your account don't have this ad set details"
                        }
                    }
                }

    def ad_level(self):
        value = 0
        find = None
        if self.__request.get('any', None):
            for camp in self.__insight:
                if self.__request.get('any').lower() in camp.get("ad_name", "").lower():
                    print("Find")
                    if self.__metric == "cpr":
                        value += (float(camp.get('spend', 0)) /
                                  int(camp.get('estimated_ad_recallers', 0)))
                    elif self.__metric == "roas":
                        value += float(camp.get('mobile_app_purchase_roas', 0)) + \
                            float(camp.get('website_purchase_roas', 0))
                    elif self.__metric == "roi":
                        value += (
                            float(camp.get('cost_per_total_action', 0)) -
                            float(camp.get('spend', 0))) / float(camp.get('spend', 0))
                    else:
                        value += float(camp.get(self.__metric, 0))
                    find = camp.get('ad_name')
                    break
            if find:
                return {
                    "payload": {
                        "slack": {
                            "text": "{}'s Facebook {} for {} ad {} £{}".format(
                                self.__request['period'].title(
                                ), self.__metric.upper(),
                                find,
                                tense(self.__request),
                                "{0:.2f}".format(value)
                            )
                        }
                    }
                }
            else:
                return {
                    "payload": {
                        "slack": {
                            "text": "It's seems either you misspelled ad or your account don't have this ad details"
                        }
                    }
                }

    def response(self):
        value = 0
        for camp in self.__insight:
            if self.__metric == "cpr":
                value += (float(camp.get('spend', 0)) /
                          int(camp.get('estimated_ad_recallers', 0)))
            elif self.__metric == "roas":
                value += float(camp.get('mobile_app_purchase_roas', 0)) + \
                    float(camp.get('website_purchase_roas', 0))
            elif self.__metric == "roi":
                value += (
                    float(camp.get('cost_per_total_action', 0)) -
                    float(camp.get('spend', 0))) / float(camp.get('spend', 0))
            else:
                value += float(camp.get(self.__metric, 0))
        return {
            "payload": {
                "slack": {
                    "text": "{}'s Facebook {} {} £{}".format(
                        self.__request['period'].title(),
                        self.__metric.upper(),
                        tense(self.__request),
                        "{0:.2f}".format(value)
                    )
                }
            }
        }
