from django.shortcuts import render
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.db.models import Count, Q
from django.db.models.functions import ExtractMonth
from django.views import View
from rest_framework_jwt.utils import jwt_payload_handler
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.generics import CreateAPIView, RetrieveUpdateAPIView, ListAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import detail_route
from rest_framework.viewsets import ViewSet, ModelViewSet, GenericViewSet
from datetime import datetime
import time
import jwt
from calendar import month_name
from math import ceil
from cardconnect.views import CardConnectAuth
from .models import *
from .serializers import *
from .signals import *
from .tasks import *
from .initials import InitialImageCreation
from .qrgenerator import QRCodeGenerator
from .haversine import Haversine
from .decryption import Decryption
from .stripe import StripePayment
from .phantom import HTMLtoPng



# Create your views here.
# View/Controller to save the Mobile Number and Country Code in User/MobileNumber Table
# OTP creation and Sending is aloso done in the same
class MobileOTPView(CreateAPIView):
    """docstring for mobileOTPView"""
    queryset = MobileNumber.objects.all()
    serializer_class = MobileNumberSerializer
    permission_classes = (AllowAny,)

    def create(self, request, format='json'):
        params = request.data
        try:
            user = MobileNumber.objects.get(
                mobile_number=params['mobile_number'])
            user.otp_creation()
            # user.otp_send()
            return Response({"user_id": user.id},
                            status=status.HTTP_201_CREATED)
        except BaseException:
            # Hard Coded Password to get the request.user in View
            params["password"] = time.time()
            serializer = self.get_serializer(data=params)
            serializer.is_valid(raise_exception=True)
            data = self.perform_create(serializer, params)
            return Response(data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer, params):
        user_mobile = serializer.save()
        user_mobile.otp_creation()
        # user_mobile.otp_send()
        return {"user_id": user_mobile.id}


# View/Controller to Resend the OTP
class ResendOtpView(APIView):
    """docstring for otpVerificationView"""
    permission_classes = (AllowAny,)

    def get(self, request, pk=None, format=None):
        params = request.data
        queryset = MobileNumber.objects.all()
        user = get_object_or_404(queryset, id=pk)
        user.otp_creation()
        # user.otp_send()
        return Response({"user_id": user.id}, status=status.HTTP_200_OK)


# View/Controller for OTP verification
class OtpVerificationView(APIView):
    """docstring for otpVerificationView"""
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        params = request.data
        queryset = MobileNumber.objects.all()
        user = get_object_or_404(queryset, id=params['user_id'])
        verification_test = user.otp_verification(params['otp'])
        if verification_test:
            if 'device' in params:
                if params['device']['device_type'] == "Android":
                    user.add_android_device(params['device']['device_id'])
                else:
                    user.add_ios_device(params['device']['device_id'])
            return Response({"user_id": user.id}, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)


# To update Work with PUT or PATCH type API and to retrieve work with GET
# none other than that.
class UpdateRetrieveProfileView(RetrieveUpdateAPIView):
    """docstring for updateProfileView"""
    queryset = MobileNumber.objects.all()
    serializer_class = MobileNumberSerializer
    permission_classes = (AllowAny,)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()  # To get the user from the Pk in URL
        token = instance.create_jwt()
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response({"data": serializer.data, "token": token},
                        status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        user = serializer.save()
        if "password" in serializer.validated_data:
            user.set_password(serializer.validated_data['password'])
        if "user_image" in serializer.validated_data:
            user.image_type = "Image"
            print("user Image", user.user_image)
        if "first_name" and "last_name" in serializer.validated_data and user.image_type == "Initial" and "user_image" not in serializer.validated_data:
            initals_obj = InitialImageCreation(
                serializer.validated_data['first_name'][0] +
                serializer.validated_data['last_name'][0])
            user.user_image = initals_obj.imageCreation()
        user.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()  # To get the user from the Pk in URL
        serializer = self.get_serializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)


# View/Controller for forgot Password
class ForgotPasswordView(ViewSet):
    """docstring for forgotPasswordView"""
    permission_classes = (AllowAny,)

    def retrieve(self, request):
        params = request.data
        queryset = MobileNumber.objects.all()
        try:
            user = MobileNumber.objects.get(email=params['email'])
            user.otp_creation()
            # user.forgot_password_otp_send()
            return Response(status=status.HTTP_200_OK)
        except BaseException:
            return Response({"message": "User not Exists"},
                            status=status.HTTP_400_BAD_REQUEST)


# View/Controller for Login Process
class LoginView(ModelViewSet):
    """docstring for loginView"""
    serializer_class = MobileNumberSerializer
    permission_classes = (AllowAny,)

    def retrieve(self, request, pk=None):
        queryset = MobileNumber.objects.all()
        params = request.data
        try:
            user = MobileNumber.objects.get(Q(email__iexact=params['email'])|Q(login_id=params['email']))
        except BaseException:
            return Response({"message": "Authentication failed"},
                            status=status.HTTP_400_BAD_REQUEST)
        if "password" in params:
            password_auth = user.check_password(params['password'])
            if password_auth:
                serializer = self.get_serializer(user)
                token = user.create_jwt()
                if 'device' in params:
                    if params['device']['device_type'] == "Android":
                        user.add_android_device(params['device']['device_id'])
                    else:
                        user.add_ios_device(params['device']['device_id'])
                if "type" in params and user.user_type == params['type']:
                    return Response(
                        {"data": serializer.data, "token": token}, status=status.HTTP_200_OK)
                elif "type" in params and user.user_type != params['type']:
                    return Response({"message": "You are a customer and you can't Logged in into Attendant"},
                            status=status.HTTP_400_BAD_REQUEST)
                user_payment = user.payments.all()
                if not len(user_payment):
                    return Response({"data": serializer.data,
                                     "token": token},
                                    status=status.HTTP_402_PAYMENT_REQUIRED)
                return Response(
                    {"data": serializer.data, "token": token}, status=status.HTTP_200_OK)
            else:
                return Response({"message": "Authentication failed"},
                                status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)


class PaymentMethodView(CreateAPIView):
    """docstring for paymentMethodView"""
    queryset = UserPaymentMethods.objects.all()
    serializer_class = PaymentSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request, format=None):
        params = request.data
        params["user"] = request.user.id
        serializer = self.get_serializer(data=params)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        if serializer.validated_data['default_card']:
            count = self.request.user.payments.count()
            if count:
                previously_active = self.request.user.payments.get(
                    default_card=True)
                previously_active.default_card = False
                previously_active.save()
        serializer.save()


class ListPaymentView(ModelViewSet):
    """docstring for retrievePaymentView"""
    serializer_class = RetrievePaymentSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return self.request.user.payments.all().order_by("-default_card")


class UpdateRetrievePaymentView(RetrieveUpdateDestroyAPIView):
    """docstring for detailPaymentView"""
    queryset = UserPaymentMethods.objects.all()
    serializer_class = PaymentSerializer
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()  # To get the user from the Pk in URL
        serializer = self.get_serializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()  # To get the user from the Pk in URL
        params = request.data
        params["user"] = request.user.id
        serializer = self.get_serializer(instance, data=params, partial=False)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        try:
            defaut_exists = self.request.user.payments.get(default_card=True)
            defaut_exists.default_card = False
            defaut_exists.save()
        except Exception as e:
            pass
        serializer.save()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        count = self.request.user.payments.count()
        if count > 1:
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(
                {
                    "message": "Looks like this card is your only payment method. You’ll need to add a new card before you can delete this one. No worries, we can help with that."},
                status=status.HTTP_200_OK)

    def perform_destroy(self, instance):
        instance.delete()
        update_payment = self.request.user.payments.first()
        update_payment.default_card = True
        update_payment.save()


class NearByValet(GenericViewSet):
    """docstring for nearByValet"""
    serializer_class = ValetatSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        near_valet = []
        params = request.data
        distanceRange = params['range'] * \
            1609.34 if 'range' in params and params['range'] else 2 * 1609.34
        queryset = ValetAt.objects.raw('''
			SELECT valetat.id, earth_distance(ll_to_earth({},{}), ll_to_earth(valetat.latitude, valetat.longitude)) as distance FROM valetat WHERE earth_box(ll_to_earth({},{}),{}) @> ll_to_earth(valetat.latitude, valetat.longitude) ORDER BY distance ASC;
		'''.format(params["latitude"], params["longitude"], params["latitude"], params["longitude"], distanceRange))
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class PromoCodeBalance(GenericViewSet):
    """docstring for promoCodeBalance"""
    queryset = PromoCodeBalance.objects.all()
    serializer_class = PromoCodeSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        params = request.data
        queryset = self.request.user.promo_owner.all().order_by("-created_at")
        balance = queryset.aggregate(Sum('amount'))
        serializer = self.get_serializer(queryset, many=True)
        return Response({"data": serializer.data,
                         "balance": balance['amount__sum']},
                        status=status.HTTP_200_OK)


class NotificationList(GenericViewSet):
    """docstring for notificationList"""
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        params = request.data
        queryset = self.request.user.notification_for.filter(
            is_seen=False).order_by("-created_at")
        count = queryset.count()
        serializer = self.get_serializer(queryset, many=True)
        return Response({"data": serializer.data, "count": count},
                        status=status.HTTP_200_OK)


class NotificationRead(RetrieveUpdateDestroyAPIView):
    """docstring for notificationRead"""
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
    permission_classes = (IsAuthenticated,)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()  # To get the user from the Pk in URL
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class GetStarted(CreateAPIView):
    """docstring for GetStarted"""
    queryset = GetStartedRequest.objects.all()
    serializer_class = GetStartedRequestSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request):
        params = request.data
        params["user"] = request.user.id
        serializer = self.get_serializer(data=params)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        serializer.save()


class PaymentProcess(CreateAPIView):
    """docstring for PaymentProcess"""
    queryset = PaymentRecords.objects.all()
    serializer_class = PaymentProcessSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request):
        params = request.data
        params["user"] = request.user.id
        card = request.user.payments.get(default_card=True)
        # paymentObj = StripePayment(params, request.user)
        # stripe_response = paymentObj.payment()
        total_amount = float(params['parking_fee'])+float(params['valet_tip']) if params['valet_tip'] else 00.00
        cardConnectObj = CardConnectAuth(card['card_number'],str(card['exp_month'])+str(card['exp_year']),total_amount)
        cardConnect_response = cardConnectObj.createCharge()

        if cardConnect_response:
            params['card'] = card.id
            serializer = self.get_serializer(data=params)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            return Response(status=status.HTTP_201_CREATED)
        return Response({"message": "Payment Unsuccessfull"},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def perform_create(self, serializer):
        payment = serializer.save()
        # conversion = HTMLtoPng(payment.id,payment)
        # conversion.conversionprocess()
        paymentReceipt.delay(payment.id)


class History(GenericViewSet):
    """docstring for History"""
    serializer_class = PaymentRecordsSerializer
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        now = datetime.now()
        params = request.data
        historyArray = {}
        queryset = PaymentRecords.objects.annotate(
            month=ExtractMonth('created_at')).values('month').annotate(
            history=Count('id')).order_by('month')
        for q in queryset:
            query = PaymentRecords.objects.filter(created_at__month=q['month'])
            historyArray[month_name[q['month']].upper()] = self.get_serializer(
                query, many=True).data
        return Response(historyArray, status=status.HTTP_200_OK)


class HistoryDetails(RetrieveAPIView):
    """docstring for HistoryDetails"""
    queryset = PaymentRecords.objects.all()
    serializer_class = PaymentRecordDetailSerializer
    permission_classes = (IsAuthenticated,)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()  # To get the user from the Pk in URL
        serializer = self.get_serializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)


class PaymentReceipt(View):
    """docstring for PaymentReceipt"""

    def get(self, request, pk=None):
        receipt = PaymentRecords.objects.get(id=pk)
        return render(request, 'receipt.html', {"receipt": receipt})


