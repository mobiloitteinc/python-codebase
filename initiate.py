from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.adaccount import AdAccount
from datetime import date
from .cpc import CPC
from .cpm import CPM
from .cpr import CPR
from .roas import ROAS
from .cpa import CPA
from .roi import ROI
from .cpe import CPE
from .reach import Reach
from .impression import Impressions
from .click import Clicks
from .frequency import Frequency


class InititeFacebook:
    """docstring for InititeFacebook
    """

    def __init__(self, request, user):
        super(InititeFacebook, self).__init__()
        self.__request = request
        access_token = 'EAAQ1vWSED4sBAPURFRIJUlt0wlVfZBjOetVGzerWe2ab93m0x9n9FKBs2wWv439VY4Sw9TAHknL3hNWMlZBKWOb14oXu5ped1G8ysbH4HLUvyuiARxEujShyIHZCg2TZC1RTAlSxVJoXc6RJ7W6DkPnZAZCDnjEtOKh7ZC6CGvxBMYYSRpSiICz'
        try:
            ad_account_id = user.account.facebook
        except:
            ad_account_id = "act_275312426537405"

        app_secret = 'da95ad696cd73c7cf8335010bebda40d'
        app_id = '1184987458310027'
        FacebookAdsApi.init(
            access_token=access_token
        )
        self.__account = AdAccount(ad_account_id)

    def start(self):
        req = self.__request
        if req['performance_metric'] == 'CPM':
            obj = CPM(self.__account, self.__request)
            return obj.cpm()

        elif req['performance_metric'] == 'CPC':
            obj = CPC(self.__account, self.__request)
            return obj.cpc()

        elif req.get('performance_metric') == 'CPR':
            obj = CPR(self.__account, self.__request)
            return obj.cpr()

        elif req.get('performance_metric') == 'CPA':
            obj = CPA(self.__account, self.__request)
            return obj.cpa()

        elif req.get('performance_metric') == 'ROAS':
            obj = ROAS(self.__account, self.__request)
            return obj.roas()

        elif req.get('performance_metric') == 'ROI':
            obj = ROI(self.__account, self.__request)
            return obj.roi()

        elif req.get('performance_metric') == 'CPE':
            obj = CPE(self.__account, self.__request)
            return obj.cpe()

        elif req.get('performance_metric') == 'Reach':
            obj = Reach(self.__account, self.__request)
            return obj.reach()

        elif req.get('performance_metric') == 'Impressions':
            obj = Impressions(self.__account, self.__request)
            return obj.impressions()

        elif req.get('performance_metric') == 'Click':
            obj = Clicks(self.__account, self.__request)
            return obj.click()

        elif req.get('performance_metric') == 'Frequency':
            obj = Frequency(self.__account, self.__request)
            return obj.frequency()

        elif req.get('performance_metric') == 'COS':
            return {"payload": {"slack": {"text": "My Facebook COS Result  " + str(self.date_output(req)) + "."}}}
