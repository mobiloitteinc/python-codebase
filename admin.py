from django.contrib import admin
from django.utils.safestring import mark_safe
from django.contrib.auth.models import Group,User
# from social.apps.django_app.default.models import Association, Nonce, UserSocialAuth

# admin.site.unregister(Association)
# admin.site.unregister(Nonce)
# admin.site.unregister(UserSocialAuth)

from .models import *
from .forms import *
# Register your models here.

admin.site.empty_value_display = '-'

class UserModelAdmin(admin.ModelAdmin):
	form=UserSignUp
	icon='<i class="material-icons">people</i>'
	list_display=('email','user_type','first_name','last_name','image')
	list_filter=('user_type',)
	fields = ('email', 'password', 'first_name', 'last_name', 'avatar','user_type')

	def image(self, obj):
		if obj.avatar:
			return mark_safe('<img src={} style=height:70px width=70px;></img>'.format(obj.avatar.url))

admin.site.register(MyUser,UserModelAdmin)

# class UserSubSkillAdmin(admin.ModelAdmin):
# 	list_display=('user','subSkill')
# 	list_filter=('user',)

# admin.site.register(UserSubSkills,UserSubSkillAdmin)

# class SubSkillAdmin(admin.ModelAdmin):
# 	list_display=('name','skill')
# 	list_filter=('skill',)

# admin.site.register(SubSkill,SubSkillAdmin)

admin.site.register(Skills)
admin.site.unregister(Group)