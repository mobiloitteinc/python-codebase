from django.db import models
from django.conf import settings
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from rest_framework_jwt.utils import jwt_payload_handler
from cloudinary.models import CloudinaryField
from push_notifications.models import APNSDevice, GCMDevice
from templated_email import send_templated_mail
import pyotp
import jwt
from GetValet.twilio import send_sms 


# Create your models here.
#BaseManager Class to manage the Normal and Admin User
class MyUserManager(BaseUserManager):
	# def create_user(self,email, otp=None, password=None):
	# 	mobile = self.model(email=email)
	# 	mobile.set_password(password)
	# 	mobile.save(using=self._db)
	# 	return mobile

	def create_superuser(self, email, password): 
		"""
		Creates and saves a superuser with the given mobile number and password.
		"""
		user = self.model(email=email)
		user.set_password(password)
		totp = pyotp.TOTP("JBSWY3DPEHPK3PXP",digits=4)
		otp = totp.now()
		user.otp = otp
		user.is_superuser = True
		user.is_staff = True
		user.save(using=self._db)
		return user


		
#Base User Table used same for Authentication Purpose
class MobileNumber(AbstractBaseUser, PermissionsMixin):
	"""docstring for MobileNumber"""
	#Choice Field for User Type
	USER_CHOICES = (
		('Customer', 'Customer'),
		('Attendant', 'Attendant')
	)
	user_type = models.CharField(('User Type'),max_length=10,blank=True,default='Customer',choices=USER_CHOICES)
	first_name = models.CharField(('First Name'),max_length=20,blank=True,null=True)
	last_name = models.CharField(('Last Name'),max_length=20,blank=True,null=True)
	'''Choice Field for User Image.
	if Image is not selected the Image
	with Intital created on the basis of this Field'''
	TYPE_CHOICES = (
		('Initial', 'Initial'),
		('Image', 'Image')
	)
	image_type = models.CharField(('Image Type'),max_length=20,blank=True,default='Initial',choices=TYPE_CHOICES)
	user_image = CloudinaryField('User Image',blank=True,null=True)
	email = models.EmailField(('Email'), max_length=50, blank=True,null=True,unique=True)
	login_id = models.CharField(('ID for Login'), max_length=8, blank=True,null=True,unique=True)
	country_code = models.CharField(('Country Code'),max_length=5,blank=False)
	mobile_number = models.CharField(('Mobile Number'),max_length=20,blank=False,unique=True)
	otp = models.IntegerField(('OTP'), blank=True,null=True)
	otp_active = models.BooleanField(('OTP Status'), default=False,blank=True)
	is_staff = models.BooleanField(('Staff'), default=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	objects = MyUserManager()
	# Field used for Authentication Purpose
	USERNAME_FIELD = 'email'

	# Field should be display as Row Object in Admin Panel
	def __str__(self):
		return self.email if self.email else self.mobile_number

	#Function for creating the OTP
	def otp_creation(self):
		totp = pyotp.TOTP("JBSWY3DPEHPK3PXP",digits=4)
		otp = totp.now()
		self.otp = otp
		self.save()
		return otp

	#Function for verification Purpose
	def otp_verification(self,otp):
		if self.otp == otp:
			self.otp_active = True
			self.save()
			return True
		return True

	#Function for creating JWT for Authentication Purpose
	def create_jwt(self):
		payload = jwt_payload_handler(self)
		token = jwt.encode(payload, settings.SECRET_KEY)
		auth_token = token.decode('unicode_escape')
		return auth_token

	#Function for sending OTP at the time Registration
	def otp_send(self):
		message = "Your one time password for GVallet Account Registration Verification is: "+str(self.otp)
		# send_sms(self.country_code+self.mobile_number,message)
		send_sms("+1 678-557-2219",message)

	#Function for sending OTP at the time of Forgot Password
	def forgot_password_otp_send(self):
		message = "Your one time password for GVallet Forgot Password is: "+str(self.otp)
		# send_sms(self.country_code+self.mobile_number,message)
		send_sms("+1 678-557-2219",message)

	#Function for Sending Attendee Registration Email
	def attendee_registration(self,unhashedpassword):
		send_templated_mail(template_name='AttendeeRegistration',
			from_email=settings.VALET_EMAIL_HOST_USER,
			auth_user=settings.VALET_EMAIL_HOST,
			auth_password=settings.VALET_HOST_PASSWORD,
			recipient_list=[self.email],
			context={'email':self.email,"password":unhashedpassword}
		)

	# Add Android Device for Sending Notifications
	def add_android_device(self,device_id):
		has_device_id = GCMDevice.objects.filter(registration_id=device_id).exclude(user=self)
		if has_device_id:
			has_device_id.delete()
		has_user = GCMDevice.objects.filter(user=self).exclude(registration_id=device_id)
		if has_user:
			has_user.delete()
		try:
			return GCMDevice.objects.get(registration_id=device_id,user=self)
		except Exception as e:
			return GCMDevice.objects.create(registration_id=device_id,user=self,name=self.mobile_number)

	# Add Android Device for Sending Notifications
	def add_ios_device(self,device_id):
		has_device_id = APNSDevice.objects.filter(registration_id=device_id).exclude(user=self)
		if has_device_id:
			has_device_id.delete()
		has_user = APNSDevice.objects.filter(user=self).exclude(registration_id=device_id)
		if has_user:
			has_user.delete()
		user_device = APNSDevice.objects.filter(registration_id=device_id,user=self)
		try:
			return APNSDevice.objects.get(registration_id=device_id,user=self)
		except Exception as e:
			return APNSDevice.objects.create(registration_id=device_id,user=self,name=self.mobile_number)

	# Remove Device to Stop sending Notification
	def remove_device(self,device_type,device_id):
		if device_type=="Android":
			delete_device = GCMDevice.objects.filter(user=self,registration_id=device_id).delete()
		else:
			delete_device = APNSDevice.objects.filter(user=self,registration_id=device_id).delete()
		return delete_device
	
	#To provide Permission to Staff User/Sub User
	def has_perm(self, perm, obj=None):
		return self.is_staff

	#To provide User Module Permission
	def has_module_perms(self, app_label):
		return self.is_superuser

	#META Configuration of Model/Table
	class Meta:
		"""docstring for meta"""
		verbose_name_plural = "User Details"

# User Payment method with Card Information
class UserPaymentMethods(models.Model):
	"""docstring for UserPaymentMethods"""
	user = models.ForeignKey(MobileNumber,on_delete=models.CASCADE,related_name="payments")
	card_holder = models.CharField(('Card Holder'),max_length=40)
	card_type = models.CharField(('Card Type'),max_length=10)
	card_number = models.BigIntegerField(('Card Number'))
	card_cvv = models.IntegerField(('Card CVV'))
	biling_address = models.CharField(('Biling Address'),max_length=500)
	city = models.CharField(('City'),max_length=20)
	state = models.CharField(('State'),max_length=20)
	zip_code = models.CharField(('Zip Code'),max_length=20)
	#Choice Field for Card Type
	TYPE_CHOICES = (
        ('Personal', 'Personal'),
        ('Business', 'Business')
    )
	payment_type = models.CharField(('Payment Type'),max_length=10,default='Personal',choices=TYPE_CHOICES)
	is_active = models.BooleanField(('Status'),default='False',blank=True)
	default_card = models.BooleanField(('Default'),default='False',blank=True)

	class Meta:
		"""docstring for meta"""
		verbose_name_plural = "User's Credit Card"


class ValetOfferServices(models.Model):
	"""docstring for ValetServicePlaces"""
	place_type = models.CharField(('Type'),max_length=40)
	active_icon = CloudinaryField('Active Icon',blank=True,null=True)
	deactive_icon = CloudinaryField('Deactive Icon',blank=True,null=True)
	

	def __str__(self):
		return self.place_type
	class Meta:
		"""docstring for meta"""
		verbose_name_plural = "Valet List"	
    		
class ValetAt(models.Model):
	"""docstring for ValetAt"""
	service_for = models.ForeignKey(ValetOfferServices,on_delete=models.CASCADE,related_name="service_at")
	name = models.CharField(('Name'),max_length=100)
	logo = CloudinaryField('Logo',blank=True,null=True)
	address = models.CharField(('Address'),max_length=100)
	latitude = models.FloatField(('Latitude'),blank=True,null=True)
	longitude = models.FloatField(('Longitude'),blank=True,null=True)
	key_space = models.IntegerField(('Space'),default=50)
	parking_price = models.FloatField(('Price'))
	image = CloudinaryField('Image',blank=True,null=True)
	google_place_id = models.CharField(('Google Place ID'),max_length=100)


	def __str__(self):
		return self.name

	class Meta:
		"""docstring for meta"""
		db_table = "valetat"
		verbose_name_plural = "All Valet List"

class PromoCodeBalance(models.Model):
	"""docstring for PromoCodeBalance"""
	user = models.ForeignKey(MobileNumber, on_delete=models.CASCADE, related_name="promo_owner")
	amount = models.IntegerField(('Amount Received'))
	FOR_CHOICES = (
		('Code', 'Code'),
		('Invite', 'Invite')
	)
	amount_via = models.CharField(('Amount via'),max_length=10,default='Invite',choices=FOR_CHOICES)
	invitee = models.ForeignKey(MobileNumber, on_delete=models.CASCADE,related_name="invitee",null=True,blank=True)
	promoCode = models.CharField(('Code'),max_length=30,null=True,blank=True)
	created_at = models.DateTimeField(('created_at'), auto_now=False, auto_now_add=True)
	def __str__(self):
		return self.user.email

	class Meta:
		"""docstring for meta"""
		verbose_name_plural = "PromoCode List"

class Notification(models.Model):
	"""docstring for Notification"""
	user = models.ForeignKey(MobileNumber, on_delete=models.CASCADE, related_name="notification_for")
	message = models.CharField(('Message'),max_length=100)
	detail = models.CharField(('Details'),max_length=100)
	is_seen = models.BooleanField(('Seen'),default=False)
	created_at = models.DateTimeField(('created_at'), auto_now=False, auto_now_add=True)
	def __str__(self):
		return self.user.email
	
	class Meta:
		"""docstring for meta"""
		verbose_name_plural = "Notifications"
		
class GetStartedRequest(models.Model):
	"""docstring for GetStartedRequest"""
	user = models.ForeignKey(MobileNumber, on_delete=models.CASCADE, related_name="requested_user",null=True,blank=True)
	user_name = models.CharField('User Name',max_length=50,null=True,blank=True)
	mobile = models.CharField('User Mobile',max_length=50,null=True,blank=True)
	attendant = models.ForeignKey(MobileNumber, on_delete=models.CASCADE, related_name="client_attendant",null=True,blank=True)
	ticket = models.IntegerField(('Ticket'))
	valet = models.ForeignKey(ValetAt, on_delete=models.CASCADE, related_name="requested_valet")
	key = models.IntegerField(('Key'), blank=True, null=True)
	STATUS_CHOICES = (
		('Parked', 'Parked'),
		('Requested', 'Requested'),
		('Retrieved', 'Retrieved'),
		('Archive', 'Archive'),
	)
	requeststatus = models.CharField(('Request Status'),max_length=20,default='Parked',choices=STATUS_CHOICES)
	created_at = models.DateTimeField(('created_at'), auto_now=False, auto_now_add=True)
	
	class Meta:
		"""docstring for meta"""
		unique_together = ("ticket", "valet")
		db_table = "getstartedrequest"
		verbose_name_plural = "Service Requests"

class PaymentRecords(models.Model):
	"""docstring for PaymentRecords"""
	user = models.ForeignKey(MobileNumber, on_delete=models.CASCADE, related_name="payment_user")
	valet_request = models.OneToOneField(GetStartedRequest, unique=True, on_delete=models.CASCADE, related_name="valet_request")
	parking_fee = models.FloatField(('Parking Fee'))
	valet_tip = models.FloatField(('Valet Tip'),null=True,blank=True,default=00.00)
	receipt_image = CloudinaryField(("Receipt Image"),null=True,blank=True)
	receipt_pdf = CloudinaryField(("Receipt PDF"),null=True,blank=True)
	card = models.ForeignKey(UserPaymentMethods, on_delete=models.CASCADE, related_name="payment_card")
	created_at = models.DateTimeField(('created_at'), auto_now=False, auto_now_add=True)

	def __str__(self):
		return self.user.email
	
	class Meta:
		"""docstring for meta"""
		verbose_name_plural = "Payment Details"




		